from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import ExpenseCategory, Account, Receipt


# def show_receipt(request, id):
#     receipt = get_object_or_404(Receipt, id=id)
#     context = {
#         "receipt_object": receipt,
#     }
#     return render(request, "", context)


def receipt_list(request):
    receipts = Receipt.objects.all()
    context = {
        "receipt_list": receipts,
    }
    return render(request, "receipts/list.html", context)
